# simulation.py
# A small BST protocol simulation tool
#
# Users specify data size (u) such that u > 0
# The number of redundant bits n - k > 0
# Bit Error Probabiity p
#
# Program Simulates the transmission of N = 1,000,000 new packets over a BST channel, 
# Each of these packets may require retransmissoin more than once
# 
# Authors: Jonathan Glassey, Hadi Roudaki 2012


from numpy import random
from packet import Packet
from scipy.misc import comb
'''
Function to Simulate the transmission of packets over a BSC Channel

u -> User Specified Data Size (u > 0)
k -> (o + u) 100 + u
n -> total packet-size(n - k >= 0)
p -> Bit Error Probability
k -> packet.length
'''
def simulation(u, r, p, N = 1000000):
   recieved = 0;
   totalEfficiency = 0;
   s = random.RandomState()
   k = 100 + u;
   n = k + r;
   validInput = False;
   x = 0;
   tStar = 0;
   
   
   if checkSimInput(u, n, k):
      validInput = True
   else:
      exit('Program Stopped Invalid Input')
   

   # Calculate t*
   tStar = tStarCalc(n, k)
   
   print "t* = " + str(int(tStar))
   
   for i in xrange(1, N):
      efficiency = 0
      transmissions = 0
      errors = 0
      
      
      # Introduce Random Number S bit errors into the code (following BSc Model)
      errors = s.binomial(n, p);      
      transmissions += 1
      
      # If errors > tStar: try resend until it is recieved
      while errors > tStar:
         errors = s.binomial(n, p);
         transmissions += 1
   
      # Efficiency Value is calculated, Packets are appended onto array for later calculation
      # Ni = u/(Xi*n)
      
      efficiency = (float(u)/(transmissions * n))
      recieved += 1
      
      x += efficiency
   
   # Final Efficiency Calculation
   # N = 1,000,000
   # 1/N * (all Ni)

   totalEfficiency = (1.0/N) * x
   print totalEfficiency;
   
'''
 Function to calculate the Hamming Bound of a given set of packets
 Returns t*
'''
      
def tStarCalc(n, k):
   
   i = 0
   right  = 0
   left = 2**(n - k)
   right += comb(n, i, exact = True)

   
   while left >= right: 
      i+=1
      right += comb(n, i, exact = True)

      
   return i-1
    
def checkSimInput(u, n, k):
   if 0 >= u:
      print('ERROR: U is not greater than 0')
      return False
   elif (n - k) < 0:
      print ('ERROR: n - k is smaller than 0')
      return False
   else:
      return True
