'''
Class for simulating a packet that is transmitted over the network
u -> User Defined Packet Size


'''
class Packet:
    
    def __init__(self, n, u, i):
        self.u = u
        self.k = 100 + u
        self.n = n
        self.i = i
        self.errors = 0;
        self.transmissions = 0;
        self.efficiency = 0
    
    def getUserDefined(self):
        return self.u;
    
    def getPacketLength(self):
        return self.k
    
    def getRedundantBits(self):
        return self.n - self.k
    
    def setEfficiency(self, e):
        self.efficiency = e
        
    def setErrors(self, f):
        self.errors = f
        
    def transmit(self):
        self.transmissions += 1