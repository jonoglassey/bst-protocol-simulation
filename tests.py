# tests.py
# file to run similations on COSC264 assignment
#
# Authors: Hadi Roudaki , Jonathan Glassey 2012
#

from simulation import * 
from part3 import * 

'''
Tests Average Efficiency of the transmission of 1 Million packets where u varies from
100 to 2000 in increments of 20 (100 Individual Tests) redundant packets = 0.1 * k

p = 0.01 (Probability of a Bit Error 1%)
'''
def sim1():
   u=100   
   while(u<=2000):
  
      r=(u+100)*0.1
      p=0.01
      simulation(u,r,p)
      u+=10
      
   print '_______END_______'
    
'''
Tests Average Efficiency of the transmission of 1 Million packets where u varies from
100 to 2000 in increments of 20 (100 Individual Tests) redundant packets = 0.1 * k

p = 0.001 (Probability of a Bit Error 0.1%)
'''
def sim2():    
   u=100   
   while(u<=2000):
   
      r=(u+100)*0.1
      p=0.001
      simulation(u,r,p)
      u+=10


'''
Tests Average Efficiency of the transmission of 1 Million packets where p increases
from 10**-5 to 10**-2 increasing in increments of 0.0001
'''
   
def simBscProbability():
   p=10**-5
   while(p<=10**-2):
 
      u =512
      r=(u)*0.1
      p+=0.0001
      simulation(u,r,p)

#######################

'''
Simulates the transmission of packets over at Two State channel where the number of
redundant bits (n - k) increases from 0 to 500 in incrememnts of 10
'''

def simTwoState():

   print "Two State Sim"
   
   r=0
   while(r!=510):

 
      u =1024

      p=10**-3
      pg=10**-5
      pb=10**-3

      twoStateSimulation(u,r,pg,pb)
      r+=10
      
simTwoState()